
DEPRECATED
-----------

This app has lost it's purpose with the introduction of Django data migrations

https://docs.djangoproject.com/en/1.7/topics/migrations/#data-migrations

If you are using Django 1.7+, it is advised that you use data migrations for initial data instead.


Django Syncdb Completed Signal
-----------

Implements a signal for Django that fires when all models have synced or migrated (depending on Django version). Suitable for creating initial data as all models are guaranteed to exist.

Installation & Configuration:
-----------

- Install using ``pip install git+https://bitbucket.org/mverleg/django_syncdb_completed.git`` (or download and copy the app into your project). 

You do not need to add anything to your installed apps.

Usage
-----------

Simply import and register the signal:

                from syncdb_completed import syncdb_completed
                
                syncdb_completed.connect(my_signal_handler)

And put your code in the my_signal_handler function (or whatever you want to call it).

Like post_syncdb and post_migrate, make sure the changes are idempotent (applying them a second time doesn't change anything).

License
-----------

django_syncdb_completed is available under the revised BSD license, see LICENSE.txt. You can do anything as long as you include the license, don't use my name for promotion and are aware that there is no warranty.



