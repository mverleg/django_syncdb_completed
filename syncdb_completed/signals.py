
"""
	Fires when all models have been syncdb'ed (or migrated).
"""

from settings import INSTALLED_APPS
from django.db.models.loading import get_app, get_models
from django.dispatch import Signal


"""
	The signal (import this one).
"""
syncdb_completed = Signal(providing_args = ['last_app', 'verbosity', 'interactive',])


"""
	Get the last application which has models. Now with simple caching.
"""
def last_app_with_models():
	if not hasattr(last_app_with_models, 'last_app_path'):
		last_app_with_models.last_app_path = None
		for app_path in reversed(INSTALLED_APPS):
			app = get_app(app_path.split('.')[-1])
			if len(get_models(app)):
				last_app_with_models.last_app_path = app_path
				break
	return last_app_with_models.last_app_path


"""
	Check if the current app is the last one.
"""
def syncdb_complete(app, app_name, verbosity, interactive, **kwargs):
	if app_name.endswith('.models'):
		app_name = app_name[:-7]
	if last_app_with_models() == app_name:
		syncdb_completed.send(sender = None, last_app = app, verbosity = verbosity, interactive = interactive)

def check_syncdb_complete(app, verbosity, interactive, **kwargs):
	syncdb_complete(app, app_name = app.__name__, verbosity = verbosity, interactive = interactive, **kwargs)

def migrate_check_syncdb_complete(signal, sender, verbosity, interactive, **kwargs):
	syncdb_complete(sender, app_name = sender.name, verbosity = verbosity, interactive = interactive, **kwargs)

"""
	Check this for every post_syncdb or post_migrate, depending on Django version.
"""
try:
	from django.db.models.signals import post_migrate
	post_migrate.connect(migrate_check_syncdb_complete)
except ImportError:
	from django.db.models.signals import post_syncdb
	post_syncdb.connect(check_syncdb_complete)


